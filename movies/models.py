from django.db import models

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "Categories"

class Movies(models.Model):
    title = models.CharField(max_length=255)
    category = models.ForeignKey(
        Category,
        related_name='movies_category',
        on_delete=models.CASCADE
    )
    overview = models.TextField(null=True, blank=True)
    poster = models.ImageField(upload_to='posters/', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "Movies"

    def __str__(self):
        return f"{self.title} - {self.category}"