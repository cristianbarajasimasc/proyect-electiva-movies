from django.urls import path
from . import views

urlpatterns = [
    path('', views.list_movies, name='movie_list'),
    path('<int:movieId>/', views.get_movie, name='get_movie')
]
