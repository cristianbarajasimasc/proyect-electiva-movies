from django.shortcuts import render
from .models import Movies
# Create your views here.

def list_movies(request):

    movies = Movies.objects.all() # SELECT * FROM movies_movies

    return render(request, 'movies/list_movies.html',{'movies':movies})


def get_movie(request,movieId):
    movie = Movies.objects.get(id=movieId) # SELECT * FROM movies_movies WHERE id = 1
    return render(request, 'movies/get_movie.html',{'movie': movie})